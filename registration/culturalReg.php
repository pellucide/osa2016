<html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!--[if IE]>
        <meta name="X-UA-Compatible" content="IE=edge" >
        <![endif]-->
        <title>Cultural Registration 2016</title>

<link rel='stylesheet' id='bootstrap-css'  href='http://osa2016.org/wp-content/plugins/easy-bootstrap-shortcodes/styles/bootstrap.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-fa-icon-css'  href='http://osa2016.org/wp-content/plugins/easy-bootstrap-shortcodes/styles/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='ct-styles-css'  href='http://osa2016.org/wp-content/plugins/woocommerce-cart-tab/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-google-font-css'  href='//fonts.googleapis.com/css?family=Ubuntu%3A300%2C400%2C500%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-bootstrap-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/bootstrap.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-blueimp-gallery-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/blueimp-gallery.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-jquery-scrollpane-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/jquery.scrollpane.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-icons-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/icon.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-layout-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/layout.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='tyler-layout-mobile-css'  href='http://osa2016.org/wp-content/themes/Tyler/css/layout-mobile.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='ebs_dynamic_css-css'  href='http://osa2016.org/wp-content/plugins/easy-bootstrap-shortcodes/styles/ebs_dynamic_css.php?ver=4.4.2' type='text/css' media='all' />
<script type='text/javascript' src='http://osa2016.org/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://osa2016.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://osa2016.org/wp-content/plugins/easy-bootstrap-shortcodes/js/bootstrap.min.js?ver=4.4.2'></script>
<link rel='https://api.w.org/' href='http://osa2016.org/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://osa2016.org/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://osa2016.org/wp-includes/wlwmanifest.xml" />


		  <script>
		  function validateCulturalForm() {

		      var x = document.forms["cultural"]["firstName"].value;
		      if (x == null || x == "") {
		          alert("First Name must be filled out");
		          return false;
		      }


		      x = document.forms["cultural"]["email"].value;
		      if(x == null || x ==""){
		          alert("Email is required'");
		          return false;
		      }

		      x = document.forms["cultural"]["phone"].value;

		       if(x == null || x ==""){
		          alert("Phone must be filled out");
		          return false;
		      }


		     x = document.forms["cultural"]["city"].value;
		      if(x == null || x ==""){
		          alert("City is required'");
		          return false;
		      }

		      x = document.forms["cultural"]["state"].value;
		      if(x == null || x ==""){
		          alert("State is required'");
		          return false;
		      }


		  }

		  </script>

	</head>
    <body >
<H2 align='center'> Cultural Registration </H2>


<div class="container-fluid">

<form role="form"  name="cultural" action="http://osa2016.org/registration/culturalRegSubmit.php" onsubmit="return validateCulturalForm()" method="post"  >
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="category" class="col-sm-12 col-md-10 col-lg-6">Event Category:</label>
                <select name="category" class="col-sm-12 col-md-10 col-lg-6">

<!--		<option value="CHAMPU CHHANDA AND ODISHI - VOCAL">Champu-Chhanda And Odishi- Vocal </option> -->
<!--                   <option value="CHAMPU CHHANDA AND ODISHI - INSTRUMENTAL">Champu-Chhanda And Odishi- Instrumental </option> -->
                  
                  <option value="BHAJAN">Bhajan and devotional song</option>
                  
<!--                  <option value="SB AWARD">Subrina Biswal Award in Performing Arts</option> -->
<!--                  <option value="OSA GOT TALENT">OSA Got Talent</option>  -->
                  <option value="MEHFIL">Mehfil</option>                                    
                </select>
			</div>
    	</div>
    </div>
    
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="" class="col-sm-12 col-md-10 col-lg-6">Number of Participants:</label>
                <select name="teamsize" class="col-sm-12 col-md-10 col-lg-6">
                  <option value="SOLO">Solo</option>
                  <option value="UP TO 4">Group up to 4</option>
                  <option value="FIVE OR MORE">Group of 5 or more</option>
                </select>
	    </div>
    	</div>
    </div>

    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="firstName">Participant(s) Name (s):</label>
                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Name(s) (comma separated )" required="true" >
            </div>
    	</div>
    </div>
    
    <!--
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="lastName">Participant's Last Name:</label>
			    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="last name">
		    </div>
        </div>
	</div>
-->
     <!--  parent-->

   <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="parent_firstName">Parent/Coordinator's First Name:</label>
                <input type="text" class="form-control" id="parent_firstName" name="parent_firstName" placeholder="parent first name" required="true" >
            </div>
    	</div>
    </div>
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="parent_lastName">Parent/Coordinator's Last Name:</label>
			    <input type="text" class="form-control" id="parent_lastName" name="parent_lastName" placeholder="parent last name" required="true">
		    </div>
        </div>
	</div>

  <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="age">Age/ Age Group:</label>
                <input type="text" class="form-control" id="age" name="age" placeholder="age" required="true">
            </div>
    	</div>
    </div>
    
  
     <!--  Email -->
	    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" name = "email" placeholder="email" required="true">
  	      </div>
    	</div>
	</div>
	
      <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-6" style="">
           <div class="form-group-sm">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="(+0)000-00-0000" required="true">
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="city">City and State:</label>
              <input type="text" class="form-control" id="city" name = "city" placeholder="city" required="true">
              <input type="text" class="form-control" id="state" name = "state" placeholder="state" required="true">
  	      </div>
    	</div>
	</div>
 
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8" style="">
          <div class="form-group-sm">
              <label for="desc">Description :</label>
              <input type="text" class="form-control" id="desc" name = "desc" placeholder="Description of the performance " required="true">
              
  	      </div>
    	</div>
 </div>   
 <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8" style="">
          <div class="form-group-sm">
              <label for="url">Sample Audio/Video Clip URL  :</label>
              <input type="url" class="form-control" id="url" name = "url" placeholder="Your media URL here. Give a common url if not applicable " required="true">
              
  	      </div>
    	</div>
	</div>
    <!--submit-->
    <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-6" style="">
               <div class="form-group-sm">
	
  <button type="submit" class="btn  btn-primary"  id="register">Submit</button>
     	        </div>
            </div>
    </div>

</form>

</div>
          </body>
</html>