<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OSA 2016 Registration</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!--  reCAPTCHA library JavaScript -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
function validateForm() {
    var x = document.forms["regForm"]["firstName"].value;
    if (x == null || x == "") {
        alert("First Name must be filled out");
        return false;
    }
    
    x = document.forms["regForm"]["lastName"].value;
    if (x == null || x == "") {
        alert("Last Name must be filled out");
        return false;
    }
    
    x = document.forms["regForm"]["email"].value;
    if(x == null || x ==""){
        alert("Email is required'");
        return false;
    }
    
    x = document.forms["regForm"]["phone"].value;
  
     if(x == null || x ==""){
        alert("Phone must be filled out");
        return false;
    }
    
  
    
    x = document.forms["regForm"]["state"].value;
    if(x == null || x ==""){
        alert("State is required'");
        return false;
    }
    
    
    x = document.forms["regForm"]["osaMemberCBox"].value;
    if(x == null || x =="Please select"){
        alert("Please respond to 'Is your OSA membership current?'");
        return false;
    }
    
}

</script>


</head>

<body>

<div class="container-fluid">


<!-- below form submit url should be relative for now it is hard coded-->
<form role="form"  name="regForm" action="http://osa2016.org/registration/registration2.php" onsubmit="return validateForm()" method="post"  >
	<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="firstName">First Name :*</label>
                <input type="text" class="form-control" id="firstName" name="firstName">
            </div>
    	</div>
    </div>
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="lastName">Last Name:*</label>
			    <input type="text" class="form-control" id="lastName" name="lastName">
		    </div>
        </div>
	</div>

     <!--  spouse-->

  <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="firstName">Spouse First Name:</label>
                <input type="text" class="form-control" id="spouseFirstName" name="spouseFirstName">
            </div>
    	</div>
    </div>
    
    
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="lastName">Spouse Last Name:</label>
			    <input type="text" class="form-control" id="spouseLastName" name="spouseLastName">
		    </div>
        </div>
	</div>
    

     <!--  Email -->
	    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="email">E-mail:*</label>
              <input type="email" class="form-control" id="email" name = "email">
  	      </div>
    	</div>
	</div>
	
      <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-3" style="">
           <div class="form-group-sm">
                <label for="phone">Phone (With Country Code):*</label>
                <input type="text" class="form-control" id="phone" name="phone">
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="email">Address:*</label>
              <input type="text" class="form-control" id="street" name = "street" placeholder="Street">
              <input type="text" class="form-control" id="city" name = "city" placeholder="City">
              <a href="#" data-toggle="tooltip" title="2 Char State Abbreviation for USA and Full state name for other countries" onclick="return false;" >
              <input type="text" class="form-control" id="state" name = "state" placeholder="State">  </a>
              <input type="text" class="form-control" id="zip" name = "zip" placeholder="Zip">
              <input type="text" class="form-control" id="country" name = "country" placeholder="Country">
  
  	      </div>
    	</div>
	</div>
    
    <!--  XXXXX -->  


<!-- accordion 1--> 


<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        <label for="children">Children:(click to expand)</label>
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              
              <input type="text" class="form-control" id="child1" name = "child1" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child2" name = "child2" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child3" name = "child3" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child4" name = "child4" placeholder="FirstName Lastname">
  	      </div>
    	</div>
	   </div>
      </div>
    </div>
  </div>
  
  <!-- accordion 2--> 
   <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
           <label for="children">Parents visiting from India:(click to expand)</label>
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse">
      <div class="accordion-inner">
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              
              <input type="text" class="form-control" id="parents1" name = "parents1" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="parents2" name = "parents2" placeholder="FirstName Lastname">


  	      </div>
  	  </div>
  	  </div>    
      </div>
    </div>
  </div>
</div>


<!--Checkbox for OSA membership-->
 <div class="row">
    <div class="col-sm-12 col-md-10 col-lg-6" style="">
      <label for="sel1">Is your OSA membership current?*</label>
      <select class="form-control" id="osaMemberCBox" name = "osaMemberCBox" >
	<option selected>Please select</option>        
	<option>No</option>
        <option>Yes</option>
      </select>    
      <br>
   </div>
</div>

    <div class="g-recaptcha" data-sitekey="6LcbVhwTAAAAAIbLXEgNGlIfkw0htzMtSU1b66eE" data-secretkey="6LcbVhwTAAAAAINMScZE4CxWQdpDj3Zf1BkJP-tr"></div>

      <!--submit-->
    <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-6" style="">
               <div class="form-group-sm">
	
  <button type="submit" class="btn btn-primary"  id="register">Continue to Register</button>
     	        </div>
            </div>
    </div>

</form>

</div>
</body>
</html>
