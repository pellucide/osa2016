<?php
/**
 * WooCommerce Template Hooks
 *
 * Action/filter hooks used for WooCommerce functions/templates
 *
 * @author 		WooThemes
 * @category 	Core
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_filter( 'body_class', 'wc_body_class' );
add_filter( 'post_class', 'wc_product_post_class', 20, 3 );

/**
 * WP Header
 *
 * @see  wc_products_rss_feed()
 * @see  wc_generator_tag()
 */
add_action( 'wp_head', 'wc_products_rss_feed' );
add_action( 'get_the_generator_html', 'wc_generator_tag', 10, 2 );
add_action( 'get_the_generator_xhtml', 'wc_generator_tag', 10, 2 );

/**
 * Content Wrappers
 *
 * @see woocommerce_output_content_wrapper()
 * @see woocommerce_output_content_wrapper_end()
 */
add_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
add_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

/**
 * Sale flashes
 *
 * @see woocommerce_show_product_loop_sale_flash()
 * @see woocommerce_show_product_sale_flash()
 */
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/**
 * Breadcrumbs
 *
 * @see woocommerce_breadcrumb()
 */
add_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

/**
 * Sidebar
 *
 * @see woocommerce_get_sidebar()
 */
add_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

/**
 * Archive descriptions
 *
 * @see woocommerce_taxonomy_archive_description()
 * @see woocommerce_product_archive_description()
 */
add_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
add_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );

/**
 * Products Loop
 *
 * @see woocommerce_result_count()
 * @see woocommerce_catalog_ordering()
 * @see woocommerce_reset_loop()
 */
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Product Loop Items
 *
 * @see woocommerce_template_loop_add_to_cart()
 * @see woocommerce_template_loop_product_thumbnail()
 * @see woocommerce_template_loop_price()
 * @see woocommerce_template_loop_rating()
 */
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

/**
 * Subcategories
 *
 * @see woocommerce_subcategory_thumbnail()
 */
add_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );

/**
 * Before Single Products Summary Div
 *
 * @see woocommerce_show_product_images()
 * @see woocommerce_show_product_thumbnails()
 */
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );

/**
 * After Single Products Summary Div
 *
 * @see woocommerce_output_product_data_tabs()
 * @see woocommerce_upsell_display()
 * @see woocommerce_output_related_products()
 */
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Product Summary Box
 *
 * @see woocommerce_template_single_title()
 * @see woocommerce_template_single_price()
 * @see woocommerce_template_single_excerpt()
 * @see woocommerce_template_single_meta()
 * @see woocommerce_template_single_sharing()
 */
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

/**
 * Product Add to cart
 *
 * @see woocommerce_template_single_add_to_cart()
 * @see woocommerce_simple_add_to_cart()
 * @see woocommerce_grouped_add_to_cart()
 * @see woocommerce_variable_add_to_cart()
 * @see woocommerce_external_add_to_cart()
 */
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
add_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );
add_action( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30 );
add_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );
add_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
add_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );

/**
 * Pagination after shop loops
 *
 * @see woocommerce_pagination()
 */
add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

/**
 * Product page tabs
 */
add_filter( 'woocommerce_product_tabs', 'woocommerce_default_product_tabs' );
add_filter( 'woocommerce_product_tabs', 'woocommerce_sort_product_tabs', 99 );

/**
 * Checkout
 *
 * @see woocommerce_checkout_login_form()
 * @see woocommerce_checkout_coupon_form()
 * @see woocommerce_order_review()
 */
add_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
add_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
add_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_order_review', 'jp_woocommerce_after_checkout_note', 20 );
function jp_woocommerce_after_checkout_note(){
    global $woocommerce;
    ?>
       <tr class="warning-block">
          <td colspan="6">
                  <p class="message">
                     <font color="black" >
                      To keep our costs low, we prefer the "Cheque Payment" option, but payment via PayPal is welcome as well.
                     </font>
                 </p>
           </td>
       </tr>
    <?php
}



/**
 * Cart
 *
 * @see woocommerce_cross_sell_display()
 * @see woocommerce_cart_totals()
 * @see woocommerce_button_proceed_to_checkout()
 */
add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
add_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
add_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );

/**
 * Footer
 *
 * @see  wc_print_js()
 * @see woocommerce_demo_store()
 */
add_action( 'wp_footer', 'wc_print_js', 25 );
add_action( 'wp_footer', 'woocommerce_demo_store' );

/**
 * Order details
 *
 * @see woocommerce_order_details_table()
 * @see woocommerce_order_details_table()
 */
add_action( 'woocommerce_view_order', 'woocommerce_order_details_table', 10 );
add_action( 'woocommerce_thankyou', 'woocommerce_order_details_table', 10 );
add_action( 'woocommerce_order_details_after_order_table', 'woocommerce_order_again_button' );

/**
 * Auth
 *
 * @see woocommerce_output_auth_header()
 * @see woocommerce_output_auth_footer()
 */
add_action( 'woocommerce_auth_page_header', 'woocommerce_output_auth_header', 10 );
add_action( 'woocommerce_auth_page_footer', 'woocommerce_output_auth_footer', 10 );



add_action( 'woocommerce_before_checkout_billing_form', 'jp_prefill_checkout_form');
function jp_prefill_checkout_form($checkout) {
        $_POST["billing_first_name"] = $_SESSION["billing_first_name"];
        $_POST["billing_last_name"] = $_SESSION["billing_last_name"];
        $_POST["billing_email"] = $_SESSION["billing_email"];
        $_POST["billing_address_1"] = $_SESSION["billing_address_1"];
        $_POST["billing_company"] = $_SESSION["billing_company"];
        $_POST["billing_country"] = $_SESSION["billing_country"];
        $_POST["billing_address_2"] = $_SESSION["billing_address_2"];
        $_POST["billing_city"] = $_SESSION["billing_city"];
        $_POST["billing_state"] = $_SESSION["billing_state"];
        $_POST["billing_postcode"] = $_SESSION["billing_postcode"];
        $_POST["billing_phone"] = $_SESSION["billing_phone"];

	$child_name = isset($_SESSION['child_name']) && !empty($_SESSION['child_name']); 
	if ($child_name) {
		$_SESSION['child_name'] = "";
	}
        //$checkout["billing_company"] = NULL;
        //echo "<code>";
        //var_dump($_POST);
        //echo "</code>";
	//foreach ( $checkout->checkout_fields['billing'] as $key => $field ) :
          //woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); 
        //endforeach;
}



add_action( 'woocommerce_before_cart_table','jp_woocommerce_before_cart_table');
//add_action( 'woocommerce_cart_contents','jp_woocommerce_before_cart_table');
function jp_woocommerce_before_cart_table(){
    global $woocommerce;
            //check if product already in cart
            if( sizeof($woocommerce->cart->get_cart()) > 0){
                $found = false;
                $osafound = false;
                foreach($woocommerce->cart->get_cart() as $cart_item_key=>$values){
                    $_product = $values['data'];

            if (stripos($_product->post->post_title, "Event Registration") !== false) {
                $found = true;
                //print "<code>";
                //var_dump($_product->post->post_title);
                //print "</code>";
            }

            if (stripos($_product->post->post_title, "OSA Membership") !== false) {
                $osafound = true;
                //print "<code>";
                //var_dump($_product->post->post_title);
                //print "</code>";
            }
                }

    if(jp_sponsorship_exists()){
        return;
    }

                if (!$osafound) {
                   ?> <tr class="warning-block">
                          <td colspan="6">
                              <div class="warning">
                                  <p class="message">

                           <font color="red">OSA Membership</font> <font> is Required to attend the convention. Please add <u><a href="http://osa2016.org/index.php/product/osa-membership/"  style="color:blue">OSA Membership item</a></u> to the cart. If you are already an OSA member, please ignore this message.
<br>
<br>

                                     </font>
                                 </p>
                               </div>
                           </td>
                       </tr>
                    <?php
                }


                if (!$found) {
                   ?> <tr class="warning-block">
                          <td colspan="6">
                              <div class="warning">
                                  <p class="message">

                  <?php if (empty($_SESSION["billing_first_name"])) { ?> 
                                     <font color="red">Event Registration</font><font> is Required to attend the convention. If you have already purchased event registration, please ignore this message. Otherwise please <u><a href="http://osa2016.org/index.php/registration/event-registration/"  style="color:blue">Register</a></u> yourself.
                  <?php } else { ?> 
                                     <font color="red">Event Registration</font><font> is Required to attend the convention. If you have already purchased event registration, please ignore this message. Otherwise please add <u><a href="http://osa2016.org/index.php/product/event-registration-for-osa-convention/"  style="color:blue">Event Registration item</a></u> to your cart.

                  <?php } ?> 
<br>
<br>
                                     </font>
                                 </p>
                               </div>
                           </td>
                       </tr>
                    <?php
                }
        }
}

 

add_action( 'woocommerce_before_cart_table','jc_woocommerce_after_cart_table');
//add_action( 'woocommerce_cart_contents','jc_woocommerce_after_cart_table');
function jc_woocommerce_after_cart_table(){
    global $woocommerce;
    $donate = isset($woocommerce->session->jc_donation) ? floatval($woocommerce->session->jc_donation) : 0;
    if(!jp_sponsorship_exists()){
        unset($woocommerce->session->jp_sponsorship);
    }

    if(!jc_donation_exsits()){
        unset($woocommerce->session->jc_donation);
    }

 

    // uncomment the next line of code if you wish to round up the order total with the donation e.g. £53 = £7 donation
    // $donate = jc_round_donation($woocommerce->cart->total );

    if(jp_sponsorship_exists()){
        return;
    }
    if(!jc_donation_exsits()){
        ?>
            <tr class="donation-block">
                <td colspan="6">
                    <div class="donation">
                        <p class="message">
OSA is run 100% by volunteers. But there are non-personnel costs. What makes a non-profit like OSA pay for expenses to host the convetion. Your Donations. Please donate as much as you can. We appreciate every donation. 
<br>
Thank You !!
</p>
                        <div class="input text">
                            <label>Donation (&dollar;):</label>
                            <input type="text"  name="jc-donation" maxlength="4" size="4" style="height:25px;" value="<?php echo $donate==0?0:$donate;?>"/>
                            <input type="submit" name="donate-btn" value="Add Donation"/>
                        </div>
                    </div>
                </td>
            </tr>
            <?php
    }
}

// capture form data and add basket item
add_action('init','jc_process_donation');
function jc_process_donation(){
    global$woocommerce;
    $donation = isset($_POST['jc-donation']) && !empty($_POST['jc-donation']) ? floatval($_POST['jc-donation']) : false;
    if($donation && isset($_POST['donate-btn'])){
        // add item to basket
        $found = false;

        // add to session
        if($donation > 0){
            $woocommerce->session->jc_donation = $donation;

            //check if product already in cart
            if( sizeof($woocommerce->cart->get_cart()) > 0){
                foreach($woocommerce->cart->get_cart() as $cart_item_key=>$values){
                    $_product = $values['data'];
                    if($_product->id == DONATE_ID)
                        $found = true;
                }

                // if product not found, add it
                if(!$found) {
                    $woocommerce->cart->add_to_cart(DONATE_ID);
                }
            }else{
                // if no products in cart, add it
                $woocommerce->cart->add_to_cart(DONATE_ID);
            }
        }
    }
}

// capture form data and add basket item
add_action('init','jc_process_robotics_product');
function jc_process_robotics_product(){
    global  $woocommerce;
    $robotics_product = isset($_POST['jc_robotics_product']) && !empty($_POST['jc_robotics_product']); 
    if (!$robotics_product) { 
        $robotics_product = isset($_SESSION['jc_robotics_product']) && !empty($_SESSION['jc_robotics_product']); 
    }
    #print "robotics_product_id =  "+ ROBOTICS_PRODUCT_ID;
    #print "robotics_product = $robotics_product";
    if ($robotics_product) { 
        // add item to basket
        $found = false;
            //check if product already in cart
            if( sizeof($woocommerce->cart->get_cart()) > 0){
                foreach($woocommerce->cart->get_cart() as $cart_item_key=>$values){
                    $_product = $values['data'];
                    if($_product->id == ROBOTICS_PRODUCT_ID)
                        $found = true;
                }

                // if product not found, add it
                if(!$found) {
                    $woocommerce->cart->add_to_cart(ROBOTICS_PRODUCT_ID);
                }
            }else{
                // if no products in cart, add it
                $woocommerce->cart->add_to_cart(ROBOTICS_PRODUCT_ID);
            }
    }
    $_POST['jc_robotics_product'] = "";
    $_SESSION['jc_robotics_product'] = "";
}



add_action('init','jp_process_sponsor');
function jp_process_sponsor(){
    global $woocommerce;
    $sponsor_amount=0;
   
    if(isset($_POST['grand-sponsor-btn'])){
        $sponsor_amount=10000;
    }

    if(isset($_POST['diamond-sponsor-btn'])){
        $sponsor_amount=5000;
    }

    if(isset($_POST['gold-sponsor-btn'])){
        $sponsor_amount=2000;
    }

    // add item to basket
    $found = false;

    // add to session
    if($sponsor_amount > 0){
        $woocommerce->session->jp_sponsorship = $sponsor_amount;

        //check if product already in cart
        if( sizeof($woocommerce->cart->get_cart()) > 0){
            foreach($woocommerce->cart->get_cart() as $cart_item_key=>$values){
                $_product = $values['data'];
                if($_product->id == SPONSOR_ID)
                    $found = true;
            }

            // if product not found, add it
            if(!$found) {
                $woocommerce->cart->add_to_cart(SPONSOR_ID);
            }
        }else{
            // if no products in cart, add it
            $woocommerce->cart->add_to_cart(SPONSOR_ID);
        }
    }
}
