<?php
include 'vars.php';

function getCurlData($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    $curlData = curl_exec($curl);
    curl_close($curl);
    return $curlData;
}
$firstName=""; $lastName=""; $spouseFirstName=""; $spouseLastName="";
$email=""; $phone=""; $street=""; $city=""; $state=""; $zip=""; $country="";
$child1=""; $child2=""; $child3=""; $child4=""; $parents1=""; $parents2="";
$msg = "";
$subject = "";

function sendemail() {
    // the message
    global $firstName, $lastName, $spouseFirstName, $spouseLastName;
    global $email, $phone, $street, $city, $state, $zip, $country;
    global $child1, $child2, $child3, $child4, $parents1, $parents2;
    global $msg;
    global $subject;

    $subject = "OSA2016-REG For ". $firstName . " " . $lastName ;
    $msg = "
        <html>
        <head>
        <title>Registration</title>
        </head>
        <body>
        <p>
        <p> Thank you for registering for the OSA 2016 Convention. 
        It will be our pleasure and privilege to host you, your family and friends during the Convention.
        <br>
        <p>We received following information for the Registration.
        <p><br> <B>Please Note:</B> Your registration will be confirmed only when your payments are received via PayPal or Check in Next step.
        </p> <br>
        <table>

        <tr>
        <td><B>First Name</B></td>
        <td> " . $firstName . "</td>
        </tr>

        <tr>
        <td><B>Last Name</B></td>
        <td> " . $lastName . "</td>
        </tr>

        <td><B>Spouse First Name</B></td>
        <td> " . $spouseFirstName . "</td>
        </tr>

        <tr>
        <td><B>Spouse Last Name</B></td>
        <td> " . $spouseLastName . "</td>
        </tr>

        <td><B>Email</B> </td>
        <td> " . $email . "</td>
        </tr>

        <tr>
        <td><B>Phone</B></td>
        <td> " . $phone . "</td>
        </tr>

        <td><B>Street</B> </td>
        <td> " . $street . "</td>
        </tr>

        <tr>
        <td><B>City<B/></td>
        <td> " . $city . "</td>
        </tr>

        <td><B>State<B></td>
        <td> " . $state . "</td>
        </tr>

        <tr>
        <td><B>zip </B></td>
        <td> " . $zip . "</td>
        </tr>

        <tr>
        <td><B>Country</B></td>
        <td> " . $country . "</td>
        </tr>

        <tr>
        <td><B>Child 1</B></td>
        <td> " . $child1 . "</td>
        </tr>

        <tr>
        <td><B>Child 2</B></td>
        <td> " . $child2 . "</td>
        </tr>


        <tr>
        <td> <B>Child 3 </B></td>
        <td> " . $child3 . "</td>
        </tr>


        <tr>
        <td> <B>Child 4</B></td>
        <td> " . $child4 . "</td>
        </tr>    


        <tr>
        <td><B>Parent 1</B></td>
        <td> " . $parents1 . "</td>
        </tr>
        <tr>
        <td><B>Parent 2</B></td>
        <td> " . $parents2 . "</td>
        </tr>

        </table>
        </body>
        </html> ";

    //Set Headers to Support HTML Email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    // More headers
    $headers .= 'From: OSA 2016 <registration@osa2016.org>' . "\r\n";
    //$headers .= 'Cc: registrar.osa2016@gmail.com' . "\r\n";
    $headers .= 'Bcc: sahupk@yahoo.com' . "\r\n";

    // send email
    mail($email . ', registrar.osa2016@gmail.com' ,$subject ,$msg,$headers);
    syslog(LOG_INFO, "Email has been sent to " . $email );
}


$captchaFailed = false;

//Get the Form Fields
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $firstName= clean_input($_POST["firstName"]);
    $lastName= clean_input($_POST["lastName"]);
    $spouseFirstName= clean_input($_POST["spouseFirstName"]);
    $spouseLastName= clean_input($_POST["spouseLastName"]);

    $email= clean_input($_POST["email"]);
    $phone= clean_input($_POST["phone"]);

    //Address Fields    
    $street= clean_input($_POST["street"]);       
    $city= clean_input($_POST["city"]);       
    $state= clean_input($_POST["state"]);       
    $zip= clean_input($_POST["zip"]);          
    $country= clean_input($_POST["country"]);    

    //Child names
    $child1= clean_input($_POST["child1"]);    
    $child2= clean_input($_POST["child2"]);    
    $child3= clean_input($_POST["child3"]);    
    $child4= clean_input($_POST["child4"]);       

    //Parents 
    $parents1 = clean_input($_POST["parents1"]);    
    $parents2 = clean_input($_POST["parents2"]);    
    $osaMemberCBox = clean_input($_POST["osaMemberCBox"]);

    $recaptcha=$_POST['g-recaptcha-response'];
    if(!empty($recaptcha)) {
        $google_url="https://www.google.com/recaptcha/api/siteverify";
        $secret='6LcbVhwTAAAAAINMScZE4CxWQdpDj3Zf1BkJP-tr';
        $ip=$_SERVER['REMOTE_ADDR'];
        $url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
        $res=getCurlData($url);
        $res= json_decode($res, true);
        //reCaptcha success check 
        if($res['success']) {
        } else {
            $captchaFailedMsg="Please re-enter your reCAPTCHA.";
            $captchaFailed = true; 
        }
    } else {
        $captchaFailedMsg="Please re-enter your reCAPTCHA.";
        $captchaFailed = true; 
    }
    session_start();
    //Set Woo Commerce variables for auto prefil
    $_SESSION["billing_first_name"] =  $firstName;
    $_SESSION["billing_last_name"] =  $lastName;

    $_SESSION["billing_email"] =  $email;
    $_SESSION["billing_address_1"] =  $street;

    $_SESSION["billing_phone"] =  $phone;
    $_SESSION["billing_city"] =  $city;
    $_SESSION["billing_postcode"] =  $zip;
    $_SESSION["billing_state"] =  $state;
}

$dao = new DAO();
$dao->checkMemberOrNot($email, $isOsaMember);
$dao->insertGuest_registry_2 ($firstName, $lastName, $spouseFirstName,
                              $spouseLastName , $email, $phone,
                              $child1, $child2, $child3, $child4,
                              $parents1, $parents2, $osaMemberId , $osaMemberCBox );


if ($captchaFailed) {
    #header('Location: https://www.fbi.gov/');
    print "<h2> Captcha Failed !</h2>";
    print '<a href="http://osa2016.org/index.php/registration/event-registration/">Try again</a>';
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
} else {
    sendemail();
    header('Location: http://osa2016.org/index.php/shop/');
    //print "msg=$msg";
    //print "<h2> Captcha successful</h2>";
}



$html1= <<<HTML1
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payments</title>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script>

function validateForm() {


}

function getRadioVal(form, name) {
    var val;
    // get list of radio buttons with specified name
    var radios = form.elements[name];

    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}


</script>
</head>

<body>
<div class="container-fluid">
    <div class="container-fluid">
       <div class="row">
<!--        <img src="images/registration.png" class="img-responsive"> -->
    <H2> Please validate the data and select Registration and Food Coupon options</H2>
       </div>
    </div>
    

    
    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">First Name:  <kbd><?php echo $firstName; echo " ";  echo $lastName; ?></kbd></label>

            </div>
        </div>
    </div>
    
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">Spouse: <kbd><?php echo $spouseFirstName; echo " "; echo $spouseLastName; ?></kbd></label>

            </div>
        </div>
    </div>
    
    

    

     <!--  Email -->
        
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Email: <kbd> <?php echo $email; ?> </kbd> </label>

            </div>
        </div>
    </div>
    
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
           <div class="form-group-sm">
                <label for="phone">Phone: <kbd> <?php echo $phone; ?></kbd></label>
                
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Address: 
                  <kbd><?php echo $street; echo $city ; echo $state;  echo ","; echo $zip ;  echo ","; echo $country;
                  ?>
                  </kbd>
              </label>
  
            </div>
        </div>
    </div>
   
<!--Children-->
     <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
               <label for="children">Child : <kbd><?php echo $child1; echo ","; echo $child2; echo "," ;echo $child3; echo ","; echo $child4;?></kbd></label>

          </div>
        </div>
    </div>
<!--Parents -->
 <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
               <label for="children">Parents : <kbd><?php echo $parents1; echo ","; echo $parents2; ?></kbd></label>

          </div>
        </div>
    </div>    
    </form>
</div>
</body>
</html>
HTML1;

?>
