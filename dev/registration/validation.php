<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payments</title>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script>

function validateForm() {
   
    
}

function getRadioVal(form, name) {
    var val;
    // get list of radio buttons with specified name
    var radios = form.elements[name];
    
    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}

function totalCalculation(){
	var sum = 100.00; // registration 
	var jul2_bf_qty = parseInt(document.getElementById("jul2_bf_qty").value);
	var jul3_bf_qty = parseInt(document.getElementById("jul3_bf_qty").value);
	var jul4_bf_qty = parseInt(document.getElementById("jul4_bf_qty").value);
	
	var jul2_lunch_qty = parseInt(document.getElementById("jul2_lunch_qty").value);
	var jul3_lunch_qty = parseInt(document.getElementById("jul3_lunch_qty").value);
	var jul4_lunch_qty = parseInt(document.getElementById("jul4_lunch_qty").value);
	
	var jul2_dinner_qty = parseInt(document.getElementById("jul2_dinner_qty").value);
	var jul3_dinner_qty = parseInt(document.getElementById("jul3_dinner_qty").value);
 
	var memberTypes = document.getElementsByName("memberType");   
	//.getElementById("memberType");
	var memberType = "";
	
	for (var i=0, len=memberTypes.length; i<len; i++) {
        if ( memberTypes[i].checked ) { // radio checked?
            memberType = memberTypes[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
	
	
	if ( memberType == "annual")
		sum = sum + 100;
	if ( memberType == "five")
		sum = sum + 150;
	if ( memberType == "life")
		sum = sum + 200;	
	
	sum = sum + ( 
	              (jul2_bf_qty + jul3_bf_qty + jul4_bf_qty)
				  * 10.00
				 )
			  + (
			      (jul2_lunch_qty + jul3_lunch_qty + jul4_lunch_qty)
				  * 15.00 
				  
			    )
			   +(
			      (jul2_dinner_qty + jul3_dinner_qty) * 15.00 
			    );
				
				
				
	document.getElementById("total_amount").innerHTML  = "" + sum ; 	
	
}//end calculate

</script>
</head>

<body>
<?php   include 'vars.php';


//Get the Form Fields
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   $firstName= clean_input($_POST["firstName"]);
   $lastName= clean_input($_POST["lastName"]);
   $spouseFirstName= clean_input($_POST["spouseFirstName"]);
   $spouseLastName= clean_input($_POST["spouseLastName"]);
   
   
   $email= clean_input($_POST["email"]);
   $phone= clean_input($_POST["phone"]);

   //Address Fields	
   $street= clean_input($_POST["street"]);	   
   $city= clean_input($_POST["city"]);	   
   $state= clean_input($_POST["state"]);	   
   $zip= clean_input($_POST["zip"]);	      
   $country= clean_input($_POST["country"]);	
   
   //Child names
   $child1= clean_input($_POST["child1"]);	
   $child2= clean_input($_POST["child2"]);	
   $child3= clean_input($_POST["child3"]);	
   $child4= clean_input($_POST["child4"]);	   
   
   //Parents 
   $parents1 = clean_input($_POST["parents1"]);	
   $parents2 = clean_input($_POST["parents2"]);	
}





?>

<?php
 
 $dao = new DAO();

 $dao->checkMemberOrNot($email, $isOsaMember);
 
 $dao->insertGuest_registry ($firstName, $lastName,  $email, $phone, $osaMemberId );
 
 
 if($isOsaMember){
 	echo "TEMP : Is a OSA member. "; 
 }else
 {
     echo "TEMP :  Is not a OSA member."; 
 }
	
?>


<div class="container-fluid">
    <div class="container-fluid">
       <div class="row">
<!--        <img src="images/registration.png" class="img-responsive"> -->
	<H2> Please validate the data and select Registration and Food Coupon options</H2>
       </div>
    </div>
    

    
    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">First Name:  <kbd><?php echo $firstName; echo " ";  echo $lastName; ?></kbd></label>

            </div>
    	</div>
    </div>
    
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">Spouse: <kbd><?php echo $spouseFirstName; echo " "; echo $spouseLastName; ?></kbd></label>

            </div>
    	</div>
    </div>
    
    

    

     <!--  Email -->
	    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Email: <kbd> <?php echo $email; ?> </kbd> </label>

  	      </div>
    	</div>
	</div>
	
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
           <div class="form-group-sm">
                <label for="phone">Phone: <kbd> <?php echo $phone; ?></kbd></label>
                
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Address: 
              	<kbd><?php echo $street; echo $city ; echo $state;  echo ","; echo $zip ;  echo ","; echo $country;
	              ?>
              	</kbd>
              </label>
  
  	      </div>
    	</div>
	</div>
   
<!--Children-->
	 <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
               <label for="children">Child : <kbd><?php echo $child1; echo ","; echo $child2; echo "," ;echo $child3; echo ","; echo $child4;?></kbd></label>

          </div>
    	</div>
	</div>
<!--Parents -->
 <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
               <label for="children">Parents : <kbd><?php echo $parents1; echo ","; echo $parents2; ?></kbd></label>

          </div>
    	</div>
	</div>    
    </form>
    
   <!-- Registration payments--> 
  <h4>Registration and Payments</h4> 
   <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
               <label for="children">Event Registration Fees(Must be included) : <kbd>$100</kbd></label> 
          </div>
    	</div>
	</div>

    <h4>Choose or upgrada OSA membership</h4>
    <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
    	Select 
         <input type="radio" id="memberType" name="memberType" value="annual"  onchange="totalCalculation()"> Annual ($100) </input>
         <input type="radio" id="memberType" name="memberType" value="five" onchange="totalCalculation()"> Five Year ($150) </input>
         <input type="radio" id="memberType" name="memberType" value="life" onchange="totalCalculation()"> Life Member ($200) </input>
    </div>
	<br/>
	
    <h4>Purchase Food Coupons:</h4>
    <h5>Saturday (July-2nd, 2015)</h5>
	<div class="row">
        <div class="col-md-4" style="background-color:lavenderblush;">July 2nd Breakfast[$10.00 each] 
        
        <label >Quantity:</label>
      <select class="form-control" id="jul2_bf_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
        
        </div>
        <div class="col-md-4" style="background-color:lavender;">July 2nd Lunch [$15.00 each] 
                <label >Quantity:</label>
      <select class="form-control" id="jul2_lunch_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>

        </div>
        <div class="col-md-4" style="background-color:lavenderblush;">July 2nd Dinner [$15.00 each] 
                <label >Quantity:</label>
      <select class="form-control" id="jul2_dinner_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>

        </div>
      </div>
      
<!--July 3rd --> 
    <h5>Sunday (July-3rd, 2015)</h5>
	<div class="row">
        <div class="col-md-4" style="background-color:lavenderblush;">Breakfast
       <label >Quantity:</label>
      <select class="form-control" id="jul3_bf_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
        </div>
        <div class="col-md-4" style="background-color:lavender;">Lunch
        <label >Quantity:</label>
      <select class="form-control" id="jul3_lunch_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
		</div>
<!-- paypall Lunch end -->
        <div class="col-md-4" style="background-color:lavenderblush;">Dinner
        <label >Quantity:</label>
      <select class="form-control" id="jul3_dinner_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
        </div>
      </div>

<!--July 4th -->     
    <h5>Monday (July-4th, 2015)</h5>
	<div class="row">
        <div class="col-md-4" style="background-color:lavenderblush;">Breakfast
        <label >Quantity:</label>
      <select class="form-control" id="jul4_bf_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
        </div>
        <div class="col-md-4" style="background-color:lavender;">Lunch
        <label >Quantity:</label>
      <select class="form-control" id="jul4_lunch_qty" onchange="totalCalculation()">
        <option value="0">0</option>
        <option value="1"> 1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5"> 5</option>
        <option value="6"> 6</option>
      </select>
        </div>
        <div class="col-md-4" style="background-color:lavenderblush;"></div>
      </div>


 <h3>Total Amount </h3> 
   <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
          <div class="form-group-sm">
$  <label id="total_amount" >100.00</label> 
          </div>
    	</div>
	</div>
      
  <h3>Payment Options</h3>     
  <div class="row">
        <div class="col-md-4" style="background-color:lavenderblush;">
        Pay by ACH (Not ready now)
        <input type="submit" onclick="ach()"  name="Pay by ACH" name="ACH"/>
        </div>
  </div>     
      
  <div class="row">
        <div class="col-md-4" style="background-color:lavenderblush;">
<H3>        Pay by Paypal  (Please enter the Total Amout above to Pay) </H3> 

<!--Paypal-->
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="BSQYVE6CLCY3A">
<input type="image" src="https://www.paypalobjects.com/webstatic/en_US/developer/docs/bm/PayNow.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form> <!--End Paypal-->

        </div>
  </div>     
      
      
    </form>


    
</div>





</body>
</html>

<?php
// the message
$subject = "OSA2016-REG For ". $firstName . " " . $lastName ;

$msg = " 
	<html>
	<head>
	<title>Registration</title>
	</head>
	<body>
	
	<table>
	<tr>
	<th>Field</th>
	<th>Value</th>
	</tr>
	<tr>
	<td>First Name</td>
	<td> " . $firstName . "</td>
	</tr>
	
	<tr>
	<td>Last Name</td>
	<td> " . $lastName . "</td>
	</tr>

	<td>Spouse First Name</td>
	<td> " . $spouseFirstName . "</td>
	</tr>
	
	<tr>
	<td>Spouse Last Name</td>
	<td> " . $spouseLastName . "</td>
	</tr>

	<td>Email/td>
	<td> " . $email . "</td>
	</tr>
	
	<tr>
	<td>Phone</td>
	<td> " . $phone . "</td>
	</tr>

   <td>street/td>
	<td> " . $street . "</td>
	</tr>
	
	<tr>
	<td>city</td>
	<td> " . $city . "</td>
	</tr>
   
    <td>state/td>
	<td> " . $state . "</td>
	</tr>
	
	<tr>
	<td>zip</td>
	<td> " . $zip . "</td>
	</tr>

	<tr>
	<td>Country</td>
	<td> " . $country . "</td>
	</tr>
   
         <tr>
	<td>Child 1</td>
	<td> " . $child1 . "</td>
	</tr>

         <tr>
	<td>Child 2</td>
	<td> " . $child2 . "</td>
	</tr>
	

         <tr>
	<td>Child 3</td>
	<td> " . $child3 . "</td>
	</tr>
	
	
	<tr>
	<td>Child 4</td>
	<td> " . $child4 . "</td>
	</tr>	
   
   
         <tr>
	<td>Parent 1</td>
	<td> " . $parents1 . "</td>
	</tr>
        <tr>
	<td>Parent 2</td>
	<td> " . $parents2 . "</td>
	</tr>
		
	</table>
	<H2>Data to Copy to Excel </H2
	
	<table>

	<td> " . $firstName . "</td>
	<td> " . $lastName . "</td>
	<td> " . $spouseFirstName . "</td>
	<td> " . $spouseLastName . "</td>
	<td> " . $email . "</td>
	<td> " . $phone . "</td>
	
	<td> " . $street . "</td>		
	<td> " . $city . "</td>        
	<td> " . $state . "</td>
	<td> " . $zip . "</td>
	<td> " . $country . "</td>
	<td> " . $child1 . "</td>
	<td> " . $child2 . "</td>
	<td> " . $child3 . "</td>
	<td> " . $child4 . "</td>
	<td> " . $parents1 . "</td>
	<td> " . $parents2 . "</td>
	</tr>
	</table>
	
	
	</body>
	</html>
";







//Set Headers to Support HTML Email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
$headers .= 'From: <webmaster@osa2016.org>' . "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

// send email
mail("sahupk@yahoo.com",$subject ,$msg,$headers);
?>