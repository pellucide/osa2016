<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OSA 2016 Registration</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
function validateForm() {
    var x = document.forms["regForm"]["firstName"].value;
    if (x == null || x == "") {
        alert("First Name must be filled out");
        return false;
    }
    
    x = document.forms["regForm"]["lastName"].value;
    if (x == null || x == "") {
        alert("Last Name must be filled out");
        return false;
    }
    
    x = document.forms["regForm"]["email"].value;
    var y = document.forms["regForm"]["phone"].value;
    if ((x == null || x == "") && (y == null || y == "") ) {
        alert("Email or Phone must be filled out");
        return false;
    }
    
}

</script>


</head>

<body>

<div class="container-fluid">
<div class="container-fluid">
   <div class="row">
	<img src="images/registration.png" class="img-responsive">
   </div>
</div>

<form role="form"  name="regForm" action="validation.php" onsubmit="return validateForm()" method="post"  >
	<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">First Name:</label>
                <input type="text" class="form-control" id="firstName" name="firstName">
            </div>
    	</div>
    </div>
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
            <div class="form-group-sm">
			    <label for="lastName">Last Name:</label>
			    <input type="text" class="form-control" id="lastName" name="lastName">
		    </div>
        </div>
	</div>

     <!--  spouse-->

  <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
            <div class="form-group-sm">
                <label for="firstName">Spouse First Name:</label>
                <input type="text" class="form-control" id="spouseFirstName" name="spouseFirstName">
            </div>
    	</div>
    </div>
    
    
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
            <div class="form-group-sm">
			    <label for="lastName">Spouse Last Name:</label>
			    <input type="text" class="form-control" id="spouseLastName" name="spouseLastName">
		    </div>
        </div>
	</div>
    

     <!--  Email -->
	    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Email:</label>
              <input type="text" class="form-control" id="email" name = "email">
  	      </div>
    	</div>
	</div>
	
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush;">
           <div class="form-group-sm">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" id="phone" name="phone">
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              <label for="email">Address:</label>
              <input type="text" class="form-control" id="street" name = "street" placeholder="Street name">
              <input type="text" class="form-control" id="city" name = "city" placeholder="City name">
              <input type="text" class="form-control" id="state" name = "state" placeholder="state">
              <input type="text" class="form-control" id="zip" name = "zip" placeholder="Zip">
              <input type="text" class="form-control" id="country" name = "country" placeholder="Country">
  
  	      </div>
    	</div>
	</div>
    
    <!--  XXXXX -->  


<!-- accordion 1--> 


<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        <label for="children">Children:(click to expand)</label>
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              
              <input type="text" class="form-control" id="child1" name = "child1" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child2" name = "child2" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child3" name = "child3" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="child4" name = "child4" placeholder="FirstName Lastname">
  	      </div>
    	</div>
	   </div>
      </div>
    </div>
  </div>
  
  <!-- accordion 2--> 
   <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
           <label for="children">Parents:(click to expand)</label>
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse">
      <div class="accordion-inner">
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavender;">
          <div class="form-group-sm">
              
              <input type="text" class="form-control" id="parents1" name = "parents1" placeholder="FirstName Lastname">
              <input type="text" class="form-control" id="parents2" name = "parents2" placeholder="FirstName Lastname">


  	      </div>
  	  </div>
  	  </div>    
      </div>
    </div>
  </div>
</div>

      <!--submit-->
    <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-6" style="background-color:lavenderblush">
               <div class="form-group-sm">
	
  <button type="submit" class="btn btn-default"  id="married">Continue to Register</button>
     	        </div>
            </div>
    </div>

</form>

</div>
</body>
</html>