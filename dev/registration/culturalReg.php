<div class="container-fluid">
<script>
function validateForm() {
   
    var x = document.forms["cultural"]["firstName"].value;
    if (x == null || x == "") {
        alert("First Name must be filled out");
        return false;
    }
    
    x = document.forms["cultural"]["lastName"].value;
    if (x == null || x == "") {
        alert("Last Name must be filled out");
        return false;
    }
    
    x = document.forms["cultural"]["email"].value;
    if(x == null || x ==""){
        alert("Email is required'");
        return false;
    }
    
    x = document.forms["cultural"]["phone"].value;
  
     if(x == null || x ==""){
        alert("Phone must be filled out");
        return false;
    }
    
  
   x = document.forms["cultural"]["city"].value;
    if(x == null || x ==""){
        alert("City is required'");
        return false;
    }
    
    x = document.forms["cultural"]["state"].value;
    if(x == null || x ==""){
        alert("State is required'");
        return false;
    }
    
    
}

</script>


<form role="form"  name="cultural" action="http://osa2016.org/registration/culturalRegSubmit.php" onsubmit="return validateForm()" method="post"  >
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="category" class="col-sm-12 col-md-10 col-lg-6">Event Category:</label>
                <select name="category" class="col-sm-12 col-md-10 col-lg-6">
                  <option value="ANTAKSHARI">Antakshyari</option>
                  <option value="ODISHI CHAMPU CHHANDA">Odisi, Champu-Chhanda</option>
                  
                  <option value="BHAJAN">Bhajan and devotional song</option>
                  
                  <option value="SB AWARD">Subrina Biswal Award in Performing Arts</option>
                  <option value="OSA GOT TALENT">OSA Got Talent</option>
                  <option value="MEHFIL">Mehfil</option>                                    
                </select>
			</div>
    	</div>
    </div>
    
<div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="" class="col-sm-12 col-md-10 col-lg-6">Number of Participants:</label>
                <select name="teamsize" class="col-sm-12 col-md-10 col-lg-6">
                  <option value="SOLO">Solo</option>
                  <option value="UP TO 4">Group up to 4</option>
                  <option value="FIVE OR MORE">Group of 5 or more</option>
                </select>
	    </div>
    	</div>
    </div>

    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="firstName">Participant(s) Name (s):</label>
                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Name (comma separated )" >
            </div>
    	</div>
    </div>
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="lastName">Participant's Last Name:</label>
			    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="last name">
		    </div>
        </div>
	</div>

     <!--  parent-->

   <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="parent_firstName">Parent/Coordinator's First Name:</label>
                <input type="text" class="form-control" id="parent_firstName" name="parent_firstName" placeholder="parent first name" >
            </div>
    	</div>
    </div>
    <div class="row">     
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
			    <label for="parent_lastName">Parent/Coordinator's Last Name:</label>
			    <input type="text" class="form-control" id="parent_lastName" name="parent_lastName" placeholder="parent last name">
		    </div>
        </div>
	</div>

  <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
            <div class="form-group-sm">
                <label for="age">Age/ Age Group:</label>
                <input type="number" class="form-control" id="age" name="age" placeholder="age">
            </div>
    	</div>
    </div>
    
  
     <!--  Email -->
	    
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" name = "email" placeholder="email">
  	      </div>
    	</div>
	</div>
	
      <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-6" style="">
           <div class="form-group-sm">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="(+0)000-00-0000">
          </div>
        </div>
      </div>  
      
      <!--Address -->
      
        <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-6" style="">
          <div class="form-group-sm">
              <label for="city">City and State:</label>
              <input type="text" class="form-control" id="city" name = "city" placeholder="city">
              <input type="text" class="form-control" id="state" name = "state" placeholder="state"  
  	      </div>
    	</div>
	</div>
 
  
   
 <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8" style="">
          <div class="form-group-sm">
              <label for="url">Sample Audio/Video Clip URL  :</label>
              <input type="text" class="form-control" id="url" name = "url" placeholder="paste your media URL here ">
              
  	      </div>
    	</div>
	</div>
    <!--submit-->
    <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-6" style="">
               <div class="form-group-sm">
	
  <button type="submit" class="btn  btn-primary"  id="register">Submit</button>
     	        </div>
            </div>
    </div>

</form>

</div>